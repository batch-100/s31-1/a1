import React, { Component } from "react";

class Classprops extends Component {
  render() {
    return (
      <div>
        <h1>
          Hello I'm, {this.props.name}, I'm {this.props.age}{" "}
        </h1>
      </div>
    );
  }
}

export default Classprops;
