import React from 'react';
import {Navbar, Nav} from 'react-bootstrap';

//use stateful 
//navbar function
//wala lang ung function name na to haha
//make sure lang na tama ung file name para mag inimport, wlaang error
function NavBar() {
    return (
        <Navbar br="light" expand="lg">
            <Navbar.Brand href="#home">React Bootstrap</Navbar.Brand>  
            <Navbar.Toggle aria-controls="basic-navbar-nav" />  
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav>
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#link">Link</Nav.Link>
                    <Nav.Link href="#link">Login</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}

export default NavBar;