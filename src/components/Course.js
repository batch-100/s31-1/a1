import React, { useEffect, useState } from "react"; //import useState
import PropTypes from "prop-types";
import { Card, Button } from "react-bootstrap";

function Course({ course }) {
  const { name, description, price } = course;

  //[getters, setters], if you wanted to retrive a specific value, useState
  //if tatawagin, left print out the value, retrieval, if may icha-change value if states ung right.. kung ano ung nasaloob ng s=useState, yan ung inital value
  //count = 0
  //array destructuring

  const [count, setCount] = useState(0);
  //console.log(useState(0)); //count=0, setCount=f, inasign-nan lang ng names//
  //console.log(count);
  const [seats, setSeats] = useState(10);
  const [isOpen, setIsOpen] = useState(true);
  //setCount(100);
  //console.log(count);
  //bakit tayo gumamit ng states?
  //its because of that we have to understand kung pano gumana ang mga variables natin
  //
  //bale description, synchronous niyang niru-run ni js
  useEffect(() => {
    if (seats === 0) {
      setIsOpen(false);
    }
  }, [seats]);


  function enroll() {
    setCount(count + 1);
    setSeats(seats - 1);
    console.log("Enrollees:" + count);
  }
  if (course) {
    return (
      <Card>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Text>
            <span className="subtitle">Description:</span>
            {description}
            <br />
            <span className="subtitle">Price:</span>Php: {price}
            <br />
            <span>
              <h6>Enrollees: {count}</h6>
            </span>
            <span>
              <h6>Seats: {seats}</h6>
            </span>
          </Card.Text>
          <Card.Text>
            {isOpen ? (
              <Button variant="primary" onClick={enroll}>
                Enroll
              </Button>
            ) : (
              <Button variant="danger" disabled>
                Not Available
              </Button>
            )}
          </Card.Text>

          {/* ung enroll dito is galing sa function  */}
        </Card.Body>
      </Card>
    );
  } else {
    return "";
  }
}
//checks if the course component is getting the correct prop structure/data type
//shape method is used to check if a prop object conforms or is the same to specific "shape"/data structure/type
Course.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired
  }),
};

export default Course;
