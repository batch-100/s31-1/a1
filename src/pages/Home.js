import React, { Fragment } from "react";
import { Container } from "react-bootstrap";
import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import Courses from "./Courses";
import Login from "./Login";

function Home() {
  return (
    <Fragment>
      <Container>
        <Banner />
        <Highlights />
        <Courses/>
        <Login/>
      </Container>
    </Fragment>
  );
}

export default Home;
